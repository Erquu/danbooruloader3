﻿using DanbooruLoader3.Backend.Api.Danbooru;
using DanbooruLoader3.Backend.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3Test
{
    [TestClass]
    public class PostsListTest
    {
        [TestMethod]
        public void Test_NoParameter()
        {
            Post[] posts = Posts.List().Create();
            foreach (Post p in posts)
            {
                if (p.TagCount.HasValue && p.TagCount.Value > 0)
                    Console.WriteLine(p.Tags[0]);
            }
        }

        [TestMethod]
        public void Test_Limit()
        {
            Post[] posts = Posts.List().SetLimit(2).Create();
            Assert.AreEqual(posts.Length, 2);
        }

        [TestMethod]
        public void Test_Tags()
        {
            Post[] posts = Posts.List().AddTags("dark_skin").Create();
            foreach (Post post in posts)
            {
                if (!post.Tags.Contains("dark_skin"))
                {
                    Assert.Fail();
                    return;
                }
            }
        }
    }
}
