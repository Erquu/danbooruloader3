﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DanbooruLoader3.Backend.Api.Rest;
using DanbooruLoader3.Backend.Api.Rest.Authentication;
using JSONdotNET;

namespace DanbooruLoader3Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            RestSession rest = new RestSession(new Uri("http://danbooru.donmai.us"));
            RestRequest request = rest.CreateRequest("/posts.json?limit=1", RequestMethod.GET);
            JSONObject result = request.GetResponse();

            Console.WriteLine(result.ToString(false));
        }
    }
}
