﻿using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using DanbooruLoader3.Backend.Api.Danbooru;

namespace DanbooruLoader3.Backend.Pages
{
    public class Pool : IEnumerator<Post>, IEnumerable<Post>
    {
        public static Pool FromJson(JSONObject pool)
        {
            Pool p = new Pool();

            p.Category = pool.GetString("category");

            DateTime o;
            if (DateTime.TryParse(pool.GetString("created_at"), out o))
            {
                p.CreatedAt = o;
            }
            if (DateTime.TryParse(pool.GetString("updated_at"), out o))
            {
                p.UpdatedAt = o;
            }

            p.CreatorId = pool.GetInt("creator_id");
            p.CreatorName = pool.GetString("creator_name");
            p.Description = pool.GetString("description");

            string[] tags = pool.GetString("post_ids").Split(' ');
            List<int> ids = new List<int>();
            foreach (string tag in tags)
            {
                int result;
                if (Int32.TryParse(tag, out result))
                {
                    ids.Add(result);
                }
            }
            p.PostIds = ids.ToArray();
            p.PostCount = p.PostIds.Length;

            return p;
        }

        public string Category { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public int CreatorId { get; private set; }
        public string CreatorName { get; private set; }
        public string Description { get; private set; }
        public int Id { get; private set; }
        public bool IsActive { get; private set; }
        public bool IsDeleted { get; private set; }
        public string Name { get; private set; }
        public int PostCount { get; private set; }
        public int[] PostIds { get; private set; }

        private int postIndex;
        private Post current;

        private Pool()
        {
            this.Reset();
        }

        public Post Current
        {
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return current;
            }
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            postIndex++;
            if (postIndex >= 0 && postIndex < PostIds.Length)
            {
                current = Posts.GetById(PostIds[postIndex]);
                return true;
            }
            return false;
        }

        public void Reset()
        {
            postIndex = -1;
            current = null;
        }

        public IEnumerator<Post> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }
    }
}
