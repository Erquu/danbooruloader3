﻿using JSONdotNET;
using System;

namespace DanbooruLoader3.Backend.Pages
{
    public class Post
    {
        public int? Id { get; private set; }

        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }

        public int? UploaderId { get; private set; }
        public string UploaderName { get; private set; }

        public int? Score { get; private set; }
        public PostRating Rating { get; private set; } = PostRating.UNKNOWN;

        public bool? IsBanned { get; private set; }
        public bool? IsPending { get; private set; }
        public bool? IsFlagged { get; private set; }
        public bool? IsDeleted { get; private set; }

        public bool? HasChildren { get; private set; }

        public Uri Source { get; private set; }
        public string FileUrl { get; private set; }
        public string LargeFileUrl { get; private set; }
        public string PreviewFileUrl { get; private set; }

        public string MD5 { get; private set; }
        public int? FileSize { get; private set; }
        public string FileExtension { get; private set; }

        public int? ImageWidth { get; private set; }
        public int? ImageHeight { get; private set; }

        public string[] Tags { get; private set; }
        public string[] ArtistTags { get; private set; }
        public string[] CharacterTags { get; private set; }
        public string[] CopyrightTags { get; private set; }
        public string[] GeneralTags { get; private set; }

        public int? TagCount { get; private set; }
        public int? TagCount_Artist { get; private set; }
        public int? TagCount_Character { get; private set; }
        public int? TagCount_Copyright { get; private set; }
        public int? TagCount_General { get; private set; }

        public int? FavCount { get; private set; }
        public int? ParentId { get; private set; }
        public int? ApproverId { get; private set; }
        public string FavString { get; private set; }
        public string[] PoolStrings { get; private set; }
        public int? UpScore { get; private set; }
        public int? DownScore { get; private set; }
        public bool? HasChildrenVisible { get; private set; }

        private Post() { }
        
        public static Post FromJson(JSONObject post)
        {
            Post p = new Post();

            if (post.ContainsKey("id"))
            {
                int id = -1;
                if (Int32.TryParse(post.GetString("id"), out id))
                {
                    p.Id = id;
                }
            }

            if (post.ContainsKey("created_at"))
            {
                DateTime date = DateTime.Parse(post.GetString("created_at"));
                p.CreatedAt = date;
            }

            if (post.ContainsKey("updated_at"))
            {
                DateTime date = DateTime.Parse(post.GetString("updated_at"));
                p.UpdatedAt = date;
            }

            if (post.ContainsKey("uploader_id"))
            {
                int id = -1;
                if (Int32.TryParse(post.GetString("uploader_id"), out id))
                {
                    p.UploaderId = id;
                }
            }

            if (post.ContainsKey("score"))
            {
                int score = -1;
                if (Int32.TryParse(post.GetString("score"), out score))
                {
                    p.Score = score;
                }
            }

            if (post.ContainsKey("source"))
            {
                Uri source;
                if (Uri.TryCreate(post.GetString("source"), UriKind.Absolute, out source))
                {
                    p.Source = source;
                }
            }

            if (post.ContainsKey("md5"))
            {
                p.MD5 = post.GetString("md5");
            }

            if (post.ContainsKey("rating"))
            {
                string rating = post.GetString("rating");
                switch (rating.ToLower())
                {
                    case "e":
                        p.Rating = PostRating.EXPLICIT;
                        break;
                    case "q":
                        p.Rating = PostRating.QUESTIONABLE;
                        break;
                    case "s":
                        p.Rating = PostRating.SAFE;
                        break;
                }
            }

            if (post.ContainsKey("image_width"))
            {
                int width = -1;
                if (Int32.TryParse(post.GetString("image_width"), out width))
                {
                    p.ImageWidth = width;
                }
            }

            if (post.ContainsKey("image_height"))
            {
                int height = -1;
                if (Int32.TryParse(post.GetString("image_height"), out height))
                {
                    p.ImageHeight = height;
                }
            }

            if (post.ContainsKey("tag_string"))
            {
                p.Tags = post.GetString("tag_string").Split(' ');
                p.TagCount = p.Tags.Length;
            }

            if (post.ContainsKey("tag_string_artist"))
            {
                p.ArtistTags = post.GetString("tag_string_artist").Split(' ');
                p.TagCount_Artist = p.ArtistTags.Length;
            }

            if (post.ContainsKey("tag_string_character"))
            {
                p.CharacterTags = post.GetString("tag_string_character").Split(' ');
                p.TagCount_Character = p.Tags.Length;
            }

            if (post.ContainsKey("tag_string_copyright"))
            {
                p.CopyrightTags = post.GetString("tag_string_copyright").Split(' ');
                p.TagCount_Copyright = p.Tags.Length;
            }

            if (post.ContainsKey("tag_string_general"))
            {
                p.GeneralTags = post.GetString("tag_string_general").Split(' ');
                p.TagCount_General = p.Tags.Length;
            }

            if (post.ContainsKey("fav_count"))
            {
                int favCount = -1;
                if (Int32.TryParse(post.GetString("fav_count"), out favCount))
                {
                    p.FavCount = favCount;
                }
            }

            if (post.ContainsKey("file_ext"))
            {
                p.FileExtension = post.GetString("file_ext");
            }

            if (post.ContainsKey("parent_id"))
            {
                int pid = -1;
                if (Int32.TryParse(post.GetString("parent_id"), out pid))
                {
                    p.ParentId = pid;
                }
            }

            if (post.ContainsKey("has_children"))
            {
                bool hasChildren = false;
                if (Boolean.TryParse(post.GetString("has_children"), out hasChildren))
                {
                    p.HasChildren = hasChildren;
                }
            }

            if (post.ContainsKey("approver_id"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("approver_id"), out num))
                {
                    p.ApproverId = num;
                }
            }

            if (post.ContainsKey("tag_count_general"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("tag_count_general"), out num))
                {
                    p.TagCount_General = num;
                }
            }

            if (post.ContainsKey("tag_count_artist"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("tag_count_artist"), out num))
                {
                    p.TagCount_Artist = num;
                }
            }

            if (post.ContainsKey("tag_count_character"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("tag_count_character"), out num))
                {
                    p.TagCount_Character = num;
                }
            }

            if (post.ContainsKey("tag_count_copyright"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("tag_count_copyright"), out num))
                {
                    p.TagCount_Copyright = num;
                }
            }

            if (post.ContainsKey("file_size"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("file_size"), out num))
                {
                    p.FileSize = num;
                }
            }

            if (post.ContainsKey("fav_string"))
            {
                p.FavString = post.GetString("fav_string");
            }

            if (post.ContainsKey("pool_string"))
            {
                p.PoolStrings = post.GetString("pool_string").Split(' ');
            }

            if (post.ContainsKey("up_score"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("up_score"), out num))
                {
                    p.UpScore = num;
                }
            }

            if (post.ContainsKey("down_score"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("down_score"), out num))
                {
                    p.DownScore = num;
                }
            }

            if (post.ContainsKey("is_pending"))
            {
                bool b = false;
                if (Boolean.TryParse(post.GetString("is_pending"), out b))
                {
                    p.IsPending = b;
                }
            }

            if (post.ContainsKey("is_flagged"))
            {
                bool b = false;
                if (Boolean.TryParse(post.GetString("is_flagged"), out b))
                {
                    p.IsFlagged = b;
                }
            }

            if (post.ContainsKey("is_deleted"))
            {
                bool b = false;
                if (Boolean.TryParse(post.GetString("is_deleted"), out b))
                {
                    p.IsDeleted = b;
                }
            }

            if (post.ContainsKey("tag_count"))
            {
                int num = -1;
                if (Int32.TryParse(post.GetString("tag_count"), out num))
                {
                    p.TagCount = num;
                }
            }

            if (post.ContainsKey("is_banned"))
            {
                bool b = false;
                if (Boolean.TryParse(post.GetString("is_banned"), out b))
                {
                    p.IsBanned = b;
                }
            }

            if (post.ContainsKey("uploader_name"))
            {
                p.UploaderName = post.GetString("uploader_name");
            }

            if (post.ContainsKey("has_visible_children"))
            {
                bool b = false;
                if (Boolean.TryParse(post.GetString("has_visible_children"), out b))
                {
                    p.HasChildrenVisible = b;
                }
            }

            if (post.ContainsKey("file_url"))
            {
                p.FileUrl = post.GetString("file_url");
            }

            if (post.ContainsKey("large_file_url"))
            {
                p.LargeFileUrl = post.GetString("large_file_url");
            }

            if (post.ContainsKey("preview_file_url"))
            {
                p.PreviewFileUrl = post.GetString("preview_file_url");
            }

            return p;
        }
    }
}
