﻿using DanbooruLoader3.Backend.Api.Rest.Authentication;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Api.Rest
{
    public class RestRequest
    {
        private HttpWebRequest request;

        public bool WasSuccessful { get; private set; }

        internal RestRequest(HttpWebRequest request)
        {
            this.request = request;
        }

        public JSONObject GetResponse()
        {
            using(HttpWebResponse response = (HttpWebResponse)this.request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader rsr = new StreamReader(responseStream))
                    {
                        string content = rsr.ReadToEnd();
                        return JSON.Parse(content);
                    }
                }
            }
        }
    }
}
