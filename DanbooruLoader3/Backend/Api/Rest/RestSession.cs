﻿using DanbooruLoader3.Backend.Api.Rest.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Api.Rest
{
    public class RestSession
    {
        private Uri baseUri;
        private IAuthentication auth;

        public RestSession(Uri baseUri)
        {
            if (baseUri == null)
                throw new ArgumentNullException("baseUri is null");

            this.baseUri = baseUri;
        }
        public RestSession(Uri baseUri, IAuthentication auth) : this(baseUri)
        {
            this.auth = auth;
        }

        public void SetAuth(IAuthentication auth)
        {
            this.auth = auth;
        }

        public RestRequest CreateRequest(string relativeUri, RequestMethod method)
        {
            Uri requestUri;
            if (!Uri.TryCreate(baseUri, relativeUri, out requestUri))
            {
                throw new ArgumentException("Could not create absolute Uri");
            }

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUri);

            if (request != null)
            {
                request.Method = method.ToString();

                if (this.auth != null)
                {
                    this.auth.Apply(request);
                }

                return new RestRequest(request);
            }

            return null;
        }
    }
}
