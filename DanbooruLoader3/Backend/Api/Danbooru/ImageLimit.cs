﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Api.Danbooru
{
    public class ImageLimit : IDownloadLimit
    {
        private int limit = -1;
        private int counter = 0;

        public ImageLimit(int limit)
        {
            this.limit = limit;
        }

        public void Increment(IncrementType type)
        {
            if (type == IncrementType.Image)
            {
                counter++;
            }
        }

        public bool MoveNext()
        {
            return limit > -1 && counter < limit;
        }
    }
}
