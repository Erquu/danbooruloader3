﻿using DanbooruLoader3.Backend.Api.Rest;
using DanbooruLoader3.Backend.Api.Rest.Authentication;
using DanbooruLoader3.Backend.Pages;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DanbooruLoader3.Backend.Api.Danbooru
{
    public static class Pools
    {
        public static Pool[] Search(string poolName)
        {
            RestSession rest = Store.Get<RestSession>();

            if (rest == null)
                rest = new RestSession(new Uri("http://danbooru.donmai.us"));

            IAuthentication auth = Store.Get<IAuthentication>();
            rest.SetAuth(auth);
            
            RestRequest rq = rest.CreateRequest(String.Format("/pools.json?search[name_matches]={0}", poolName), RequestMethod.GET);

            JSONArray result = rq.GetResponse() as JSONArray;

            if (result != null)
            {
                List<Pool> pools = new List<Pool>();

                foreach (JSONObject pool in result)
                {
                    if (pool != null)
                    {
                        try
                        {
                            pools.Add(Pool.FromJson(pool));
                        }
                        catch (Exception)
                        { }
                    }
                }

                return pools.ToArray();
            }
            return null;
        }

        public static Pool SearchFirst(string poolName)
        {
            Pool[] pools = Search(poolName);
            if (pools != null && pools.Length > 0)
            {
                return pools[0];
            }
            return null;
        }
    }
}
