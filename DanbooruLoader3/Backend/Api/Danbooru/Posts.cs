﻿using DanbooruLoader3.Backend.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Api.Danbooru
{
    public static class Posts
    {
        public static PostsRequest List()
        {
            return new PostsRequest();
        }

        public static Post GetById(int id)
        {
            PostsRequest req = new PostsRequest();
            req.AddTags(String.Format("id:{0}", id));

            Post[] result = req.Create();

            if (result.Length >= 1)
            {
                return result[0];
            }
            return null;
        }
    }
}
