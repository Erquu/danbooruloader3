﻿using DanbooruLoader3.Backend.Api.Rest;
using DanbooruLoader3.Backend.Api.Rest.Authentication;
using DanbooruLoader3.Backend.Pages;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DanbooruLoader3.Backend.Api.Danbooru
{
    public class PostsRequest : IEnumerator<Post>
    {
        private List<string> tags = new List<string>();
        private int startPage = 1;
        private int page = 1;
        private int? limit;
        private IDownloadLimit dlLimit = null;

        private Post current;
        private Post[] currentList;
        private int currentPointer = -1;

        public Post Current
        {
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return current;
            }
        }

        internal PostsRequest() { }

        public PostsRequest AddTags(params string[] tags)
        {
            this.tags.AddRange(tags);
            return this;
        }

        public PostsRequest AddTags_Blacklist(params string[] tags)
        {
            this.tags.AddRange(tags.Select(tag => { return String.Format("-{0}", tag); }));
            return this;
        }

        public PostsRequest SetPage(int page)
        {
            this.page = page;
            this.startPage = this.page;

            return this;
        }

        public PostsRequest SetPostsPerPage(int limit)
        {
            this.limit = limit;
            return this;
        }

        public PostsRequest SetLimit(IDownloadLimit limit) 
        {
            dlLimit = limit;
            return this;
        }

        public Post[] Create()
        {
            StringBuilder uriBuilder = new StringBuilder();
            uriBuilder.Append("/posts.json?");

            bool hasParam = false;

            if (tags.Count > 0)
            {
                if (hasParam)
                    uriBuilder.Append("&");

                uriBuilder.Append("tags=");
                uriBuilder.Append(String.Join(" ", tags));

                hasParam = true;
            }
            
            {
                if (hasParam)
                    uriBuilder.Append("&");

                uriBuilder.Append("page=");
                uriBuilder.Append(page);

                hasParam = true;
            }

            if (limit.HasValue)
            {
                if (hasParam)
                    uriBuilder.Append("&");

                uriBuilder.Append("limit=");
                uriBuilder.Append(limit.Value);

                hasParam = true;
            }

            RestSession rest = Store.Get<RestSession>();

            if (rest == null)
                rest = new RestSession(new Uri("http://danbooru.donmai.us"));

            IAuthentication auth = Store.Get<IAuthentication>();
            rest.SetAuth(auth);

            RestRequest rq = rest.CreateRequest(uriBuilder.ToString(), RequestMethod.GET);

            JSONObject result = rq.GetResponse();

            if (result != null)
            {
                JSONArray s = result as JSONArray;
                if (s != null)
                {
                    List<Post> posts = new List<Post>();
                    foreach (JSONObject o in s)
                    {
                        posts.Add(Post.FromJson(o));
                    }
                    return posts.ToArray();
                }
            }
            return null;
        }

        public IEnumerator<Post> GetEnumerator()
        {
            return this;
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            if (dlLimit == null || dlLimit.MoveNext())
            {
                if (currentList == null || currentPointer >= currentList.Length)
                {
                    currentList = Create();
                    currentPointer = 0;
                    page++;

                    if (this.dlLimit != null)
                    {
                        this.dlLimit.Increment(IncrementType.Page);
                    }
                }

                if (currentList.Length > 0 && (dlLimit == null || dlLimit.MoveNext()))
                {
                    current = currentList[currentPointer++];
                    
                    if (this.dlLimit != null)
                    {
                        this.dlLimit.Increment(IncrementType.Image);
                    }

                    return currentList != null;
                }
            }
            return false;
        }

        public void Reset()
        {
            this.page = this.startPage;
        }
    }
}
