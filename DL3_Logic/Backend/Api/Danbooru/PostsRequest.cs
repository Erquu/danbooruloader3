﻿using DanbooruLoader3.Backend.Api.Rest;
using DanbooruLoader3.Backend.Pages;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Api.Danbooru
{
    public class PostsRequest
    {
        private List<string> tags = new List<string>();
        private int? page;
        private int? limit;

        internal PostsRequest() { }

        public PostsRequest AddTags(params string[] tags)
        {
            this.tags.AddRange(tags);
            return this;
        }

        public PostsRequest AddTags_Blacklist(params string[] tags)
        {
            this.tags.AddRange(tags.Select(tag => { return String.Format("-{0}", tag); }));
            return this;
        }

        public PostsRequest SetPage(int page)
        {
            this.page = page;
            return this;
        }

        public PostsRequest SetLimit(int limit)
        {
            this.limit = limit;
            return this;
        }

        public Post[] Create()
        {
            StringBuilder uriBuilder = new StringBuilder();
            uriBuilder.Append("/posts.json?");

            bool hasParam = false;

            if (tags.Count > 0)
            {
                if (hasParam)
                    uriBuilder.Append("&");

                uriBuilder.Append("tags=");
                uriBuilder.Append(String.Join(" ", tags));
            }

            if (page.HasValue)
            {
                if (hasParam)
                    uriBuilder.Append("&");

                uriBuilder.Append("page=");
                uriBuilder.Append(page.Value);
            }

            if (limit.HasValue)
            {
                if (hasParam)
                    uriBuilder.Append("&");

                uriBuilder.Append("limit=");
                uriBuilder.Append(limit.Value);
            }

            RestSession rest = new RestSession(new Uri("http://danbooru.donmai.us"));
            RestRequest rq = rest.CreateRequest(uriBuilder.ToString(), RequestMethod.GET);

            JSONObject result = rq.GetResponse();

            if (result != null)
            {
                JSONArray s = result as JSONArray;
                if (s != null)
                {
                    List<Post> posts = new List<Post>();
                    foreach (JSONObject o in s)
                    {
                        posts.Add(Post.FromJson(o));
                    }
                    return posts.ToArray();
                }
            }
            return null;
        }
    }
}
