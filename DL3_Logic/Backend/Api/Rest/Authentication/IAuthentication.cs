﻿using System.Net;

namespace DanbooruLoader3.Backend.Api.Rest.Authentication
{
    public interface IAuthentication
    {
        bool Apply(HttpWebRequest request);
    }
}
