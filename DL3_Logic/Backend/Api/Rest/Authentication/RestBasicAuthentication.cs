﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Api.Rest.Authentication
{
    public class RestBasicAuthentication : IAuthentication
    {
        public static RestBasicAuthentication Create(string user, SecureString pass)
        {
            return new RestBasicAuthentication(user, pass);
        }

        private string user;
        private SecureString pass;

        private RestBasicAuthentication(string user, SecureString pass)
        {
            this.user = user;
            this.pass = pass;
        }

        public bool Apply(HttpWebRequest request)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.user);
            sb.Append(":");

            string b64 = null;

            IntPtr bstr = Marshal.SecureStringToBSTR(this.pass);
            try
            {
                sb.Append(Marshal.PtrToStringBSTR(bstr));
                byte[] str = Encoding.Default.GetBytes(sb.ToString());

                sb.Clear();
                sb = null;

                b64 = String.Format("Basic {0}", Convert.ToBase64String(str));
            }
            finally
            {
                Marshal.FreeBSTR(bstr);
            }

            if (b64 != null)
            {
                request.Headers["Authorization"] = b64;
                return true;
            }
            return false;
        }
    }
}
