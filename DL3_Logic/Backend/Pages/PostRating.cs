﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanbooruLoader3.Backend.Pages
{
    public enum PostRating
    {
        EXPLICIT,
        QUESTIONABLE,
        SAFE,
        UNKNOWN
    }
}
