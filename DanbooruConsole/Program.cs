﻿using DanbooruLoader3.Backend;
using DanbooruLoader3.Backend.Api.Danbooru;
using DanbooruLoader3.Backend.Api.Rest.Authentication;
using DanbooruLoader3.Backend.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DanbooruConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                //return;
            }

            bool poolSearch = false;

            Store.Put<IAuthentication>(RestBasicAuthentication.Create("erquu", "qvOHQWMejYEP3eKK2kQ_a5JbZbiy6M_JbpVOQbgqpSU"));
            Uri baseUri = new Uri("http://danbooru.donmai.us");

            DirectoryInfo target = new DirectoryInfo("D:/danbooru/__loading/allfavs/");
            target.Create();

            int downloadNum = 0;

            string mainTag = "ordfav:erquu";

            /* TEST */

            string[] tags1 = new string[] { mainTag };

            foreach (Post post in Posts.List().SetPostsPerPage(30).AddTags(tags1))
            {
                Download(post, target, baseUri, ++downloadNum);
            }

            return;

            /* END TEST **/

            if (poolSearch)
            {
                Pool pool = Pools.SearchFirst(mainTag);

                if (pool != null)
                {
                    foreach (Post post in pool)
                    {
                        Download(post, target, baseUri, ++downloadNum);
                    }
                }
            }
            else
            {
                string[] tags = new string[]
                {
                    mainTag
                };

                PostsRequest req = Posts.List().SetPostsPerPage(30).AddTags(tags);

                for (int i = 1; i <= 50; i++)
                {
                    req.SetPage(i);
                    Post[] posts = req.Create();

                    foreach (Post post in posts)
                    {
                        Download(post, target, baseUri, ++downloadNum);
                    }
                }
            }
        }

        private static void Download(Post post, DirectoryInfo target, Uri baseUri, int downloadNum)
        {
           // string format = "{order:0000}_{MD5}.{FileExtension}";
            string format = "{MD5}.{FileExtension}";

            string fileName = ToString(post, format, downloadNum);
            string path = Path.Combine(target.FullName, fileName);

            if (!File.Exists(path))
            {
                Uri downloadUri;
                string rel = post.FileUrl;

                if (Uri.TryCreate(baseUri, rel, out downloadUri))
                {
                    Console.WriteLine("Downloading {0}", downloadUri.AbsolutePath);
                    using (WebClient c = new WebClient())
                    {
                        c.DownloadFile(downloadUri, path);
                    }
                }
            } else
            {
                Console.WriteLine("Ignoring {0}", path);
            }
        }

        private static string ToString(Post post, string format, int downloadNum)
        {
            if (format == null)
                return post.ToString();

            Regex r = new Regex(@"\{(\w+)(?:(?:\:)(0+))?\}", RegexOptions.RightToLeft);
            StringBuilder b = new StringBuilder(format);

            foreach (Match m in r.Matches(format))
            {
                if (m.Groups.Count == 3)
                {
                    b.Remove(m.Index, m.Length);

                    Type tt = post.GetType();

                    string name = m.Groups[1].Value;
                    object value = null;

                    if ("order".Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        value = downloadNum;
                    }
                    else
                    {
                        PropertyInfo pi = tt.GetProperty(name);
                        if (pi != null)
                        {
                            value = pi.GetValue(post);
                        }
                    }

                    if (value != null)
                    {
                        string strValue = value.ToString();

                        if (m.Groups[2].Length > 0)
                        {
                            b.Insert(m.Index, String.Format("{0:" + m.Groups[2].Value + "}", value));
                        }
                        else
                        {
                            b.Insert(m.Index, strValue);
                        }
                    }
                }
            }

            return b.ToString();
        }

    }
}
